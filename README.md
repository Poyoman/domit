# Domit
Generate easily DOM elements with a native JavaScript template engine.

![poyEvo logo](./misc/logos/logo.png?raw=true)

## What is Domit ?
* Domit is basically **jsx without compilation**
* A **really** lightweight library (2kB)
* Comming with a router (in progress)
* Reactive dom (in progress)