// A custom element

const superWebsiteLink = ({ color, target }) => dom.div({
	className: color,
	children: [
		dom.a({
			href: target,
			textContent: 'Get me to a super website'
		}),
	]
});

document.body.appendChild(
	dom.div({
		className: 'container',
		children: [
			superWebsiteLink({ color: 'blue', target: 'https://www.muzzic.net' }),
		]
	})
);