// TODO : reactive dom
// examples of custom elements with custom events

const htmlTags = ['a', 'abbr', 'acronym', 'address', 'applet', 'area', 'article', 'aside', 'audio',
	'b', 'base', 'basefont', 'bdi', 'bdo', 'big', 'blockquote', 'body', 'br', 'button', 'canvas',
	'caption', 'center', 'cite', 'code', 'col', 'colgroup', 'command', 'datalist', 'dd', 'del', 'details',
	'dfn', 'dir', 'div', 'dl', 'dt', 'em', 'embed', 'fieldset', 'figcaption', 'figure', 'font', 'footer',
	'form', 'frame', 'frameset', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hgroup', 'hr',
	'html', 'i', 'iframe', 'img', 'input', 'ins', 'kbd', 'keygen', 'label', 'legend', 'li', 'link', 'map',
	'mark', 'menu', 'meta', 'meter', 'nav', 'noframes', 'noscript', 'object', 'ol', 'optgroup', 'option',
	'output', 'p', 'param', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'script', 'section',
	'select', 'small', 'source', 'span', 'strike', 'strong', 'style', 'sub', 'summary', 'sup', 'table',
	'tbody', 'td', 'textarea', 'tfoot', 'th', 'thead', 'time', 'title', 'tr', 'track', 'tt', 'u', 'ul',
	'var', 'video', 'wbr'];

const dom = {};

htmlTags.forEach((tagName) => {
	dom[tagName] = (props) => build({
		tagName,
		...props,
	});
});


const build = (props) => {
	const domObject = document.createElement(props.tagName || 'div');

	Object.keys(props).forEach((attr) => {
		switch (attr) {
		case 'children':
			props.children.forEach((child) => {
				domObject.appendChild(child);
			});
			break;

		case 'style':
			Object.keys(props.style).forEach((styleProp) => {
				domObject.style[styleProp] = props.style[styleProp];
			});
			break;

		default:
			if (domObject[attr] !== undefined)
				domObject[attr] = props[attr];
			else
				domObject.setAttribute(attr, props[attr]);
			break;
		}
	});

	return domObject;
};

const fragment = () => document.createDocumentFragment();

const module = {};
module.exports = {
	dom,
	build,
	fragment,
};